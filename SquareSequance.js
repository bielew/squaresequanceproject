"use strict";
exports.__esModule = true;
exports.squareDigitsSequence = void 0;
function squareDigitsSequence(num) {
    if (+num > 650 || +num < 1) {
        console.log("Error!: input out of range (it must be between 1 and 650)");
    }
    else {
        return squareCounter(+num);
    }
    function squareCounter(num) {
        var SequenceOfSquares = [];
        var lastValue = num;
        while (!SequenceOfSquares.includes(lastValue)) {
            SequenceOfSquares.push(lastValue);
            lastValue = squareSum(lastValue);
        }
        return SequenceOfSquares.length + 1;
    }
    function squareSum(numbers) {
        var result = numberToArray(numbers);
        return result.map(function (x) { return x * x; }).reduce(function (a, b) { return a + b; }, 0);
    }
    function numberToArray(numbers) {
        return numbers.toString().split('').map(function (val) { return parseInt(val); });
    }
}
exports.squareDigitsSequence = squareDigitsSequence;
