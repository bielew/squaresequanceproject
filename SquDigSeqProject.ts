import {squareDigitsSequence} from "./SquareSequance";

let readline = require('readline');
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Hello, this application squares digits of given number, sums them and repeats the process on the result until one of the results will show second time, returning length of sequence. What number would you like to input?", function(answer) {
    console.log("Here you go, length of the sequence for the number:", (answer), ", is:", squareDigitsSequence(answer));
    rl.close();
});